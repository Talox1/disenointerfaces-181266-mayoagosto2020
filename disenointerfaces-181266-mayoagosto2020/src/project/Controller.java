package project;

import java.awt.MouseInfo;
import java.util.ArrayList;



import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Controller {
	private ArrayList<Circle> puntos = new ArrayList<Circle>();
	int contClicks = 0;
	float distanciaX = 0;
	float distanciaY = 0;
	
	float[] coordClick1 = new float[2];
	float[] coordClick2 = new float[2];
    @FXML
    private Circle circulo;

        
    @FXML
    void posicion(MouseEvent event) {
    	
    	
    	
    	
    	if(this.contClicks == 0) {
    		coordClick1[0] = (int) event.getX();
    		coordClick1[1] = (int) event.getY();
    		System.out.println(contClicks+" Mouse pressed X : Y - " + 
    				coordClick1[0] + " : " + coordClick1[1]);
    		this.contClicks = 1;
    	}else {
    		coordClick2[0] = (int) event.getX();
    		coordClick2[1] = (int) event.getY();
    		System.out.println(contClicks + " Mouse pressed X : Y - " + 
    				coordClick2[0] + " : " + coordClick2[1]);
    		this.contClicks = 0;
    		
    		this.distanciaX = Math.abs(coordClick2[0] - coordClick1[0]);
    		this.distanciaY = Math.abs(coordClick2[1] - coordClick1[1]);
    		
    		calcularTrazo(distanciaX, distanciaY);
    	}
    }
    
    private void calcularTrazo(float distanciaX, float distanciaY) {

    	float proporcionX = distanciaX / distanciaY;
    	float proporcionY = distanciaY / distanciaX;
    	System.out.println("Distancia X: "+distanciaX+"\tDistancia Y: "+distanciaY);
    	
    	
    	if(distanciaX > distanciaY) {
			int y = 0;
			System.out.println("proporcion x:"+proporcionX);
    		for(float x = 0; x<distanciaX; x+=proporcionX) {
    			circulo = new Circle();
    			circulo.setLayoutX(x);
    			circulo.setLayoutY(y);
    			circulo.setStroke(Color.BLACK);
    			puntos.add(circulo);
    			y ++;
    			System.out.println("1>>x "+x+" : y "+y);
        	}
    		pintarLinea();
    	}else {
    		float y = 0;
    		System.out.println("proporcion y:"+proporcionY);
    		for(int x = 0; x<distanciaX; x++) {
    			circulo = new Circle(x,y,proporcionY);
    			puntos.add(circulo);
    			y += proporcionY;
    			System.out.println("2>>"+x+" : "+y);
        	}
    		pintarLinea();
    	}
    }
    
    private void pintarLinea() {
    	
    	
    	System.out.println("Pintando");
    	Platform.runLater(new Runnable() {
	    	@Override
	    	public void run() {
	    		for(int i = 0; i< puntos.size(); i++) {	        				
	    			puntos.get(i).setFill(Color.ALICEBLUE);
    			}
	    	}
	    });
    }
}
